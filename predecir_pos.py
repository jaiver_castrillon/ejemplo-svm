import pandas as pd  
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC #support vector classifier class

class SVM_JUGADOR():

    def __init__(self):
        self.caracteristicas = {
            '0':'Tiro',
            '1':'Penetrador',
            '2':'Tecnica',
            '3':'Pasador',
            '4':'Tactica',
            '5':'Veloz',
            '6':'Envergadura',
            '7':'Altura',
            '8':'Potencia',
            '9':'Versatilidad',
            '10':'Ofensivo',
            '11':'Defensivo',
            '12':'Coordinacion',
            '13':'Explosion',
            '14':'Dominador',
            '15':'Reboteador',
            '16':'Pivoteador'
        } 
        
        self.posiciones = [
            "BASE",
            "ESCOLTA",
            "ALERO",
            "ALAPIVOT",
            "PIVOT"
        ]

        #LECTURA DE DATOS
        ruta_data = "data.csv"
        data_set = pd.read_csv(ruta_data)

        #SEPARACIÓN DE DATOS
        X = data_set.drop('CLASE', axis=1)  
        Y = data_set['CLASE']  
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.20)

        #USANDO KERNEL RBF
        self.svc_classifier = SVC(kernel='rbf', gamma='auto')
        
        #ENTRENAMIENTO
        self.svc_classifier.fit(X_train, Y_train) 

    def clasificar(self, datos):
        x = self.svc_classifier.predict(datos) #Debe llegar en forma: [ [ x ] ] 
        return self.posiciones[int(x)]


rc = SVM_JUGADOR()
print(rc.clasificar([[0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0]]))